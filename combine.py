import pandas as pd 
import os


with open('north.txt', 'r') as f:
    arr = [ line.split()[0] for line in f.readlines() ]

print(arr)

results = []
for file in arr:
    print('Reading', file)
    df = pd.read_csv(
        os.path.join('2017', file + '.csv'), 
        converters={ 'SOBAODANH': str }
    )

    results.append(df)

df = pd.concat(results, sort=False)

df.to_csv('2017/north.csv', index=False)

