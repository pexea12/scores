import pandas as pd 
from tqdm import tqdm
import re
import os
import csv


subjects = ['Toan', 'Van', 'NN', 'Ly', 'Hoa', 'Sinh', 'Su', 'Dia', 'GDCD']
keys = ['Toán', 'Ngữ văn', 'NN', 'Vật lí', 'Hóa học', 'Sinh học', 'Lịch sử', 'Địa lí', 'GDCD']

def process_file(filename):
    print(filename)
    if (os.path.exists(os.path.join(filename.split('.')[0] + '.csv'))):
        return

    df = pd.read_excel(filename, sheet_name='Sheet1', converters={'SOBAODANH': str})[['SOBAODANH', 'DIEM_THI']]

    for subject in subjects:
        df[subject] = ''

    for i in tqdm(range(len(df))):
        raw = df['DIEM_THI'][i]
        arr = re.split('(?<=\d)\s+', raw)
        raw_scores = [ re.split(':\s+', a) for a in arr ]

        for item in raw_scores:
            if len(item) < 2: 
                continue

            if item[0] == 'KHTN' or item[0] == 'KHXH':
                continue

            if 'Tiếng' in item[0]: 
                df['NN'] = item[1]
                continue

            for j, key in enumerate(keys):
                if item[0] == key: 
                    df.ix[i, subjects[j]] = item[1]

    df.drop('DIEM_THI', inplace=True, axis=1)
    df.to_csv(filename.split('.')[0] + '.csv', index=False)

files = os.listdir('2016')
files.sort()

for file in files:
    if 'dhhanghai' in file:
        continue
    if '.xls' in file:
        process_file(os.path.join('2016', file))