import requests
import threading
import csv
from tqdm import tqdm
from bs4 import BeautifulSoup

MARGIN = int(1e5)
STEP = int(5e3)
# MARGIN = 1000
# STEP = 1000

url = 'https://diemthi.vnexpress.net/diem-thi-nam-2017/result'
default_params = {
    'college': 0,
    'area': 2,
}

def format_num(num):
    r = num
    while len(r) < 6:
        r = '0' + r
    return r

def get_student(province_code, student_code):
    params = default_params.copy()
    params['q'] = province_code + student_code

    r = requests.get(url, params)

    if (r.status_code >= 400):
        return

    data = r.json()['data']
    soup = BeautifulSoup(data, 'html.parser')

    tds = soup.findAll('td', {
        'class': 'width_sbd',
    })

    if len(tds) == 0:
        return

    scores = [ td.text for td in tds[4:15] ]
    del scores[10]
    del scores[6]

    return [params['q']] + scores



# provinces = ['04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59', '60', '61', '62', '63', '64', '65']
provinces = ['01']

def crawl(province_code, start_index, end_index):
    global count
    global results

    failed = 0
    for i in tqdm(range(start_index, end_index)):
        count += 1
        if (count > MARGIN):
            return

        student_code = format_num(str(i))
        r = get_student(province_code, student_code)

        if r is not None:
            results.append(r)
            failed = 0
        else:
            failed += 1

        if failed == 200:
            return


for province_code in provinces[:1]:
    count = 0
    results = []
    threads = []

    print(province_code, '...')

    for i in range(0, MARGIN, STEP):
        threads.append(threading.Thread(target=crawl, args=(province_code, i, i + STEP)))

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()

    print(count)

    with open(province_code + '-2017.csv', 'a') as file:
        writer = csv.writer(file, delimiter=',')
        writer.writerows(results)
