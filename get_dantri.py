import requests
import csv
import re
import threading
from tqdm import tqdm
from bs4 import BeautifulSoup

MARGIN = int(4e4)
STEP = int(5e3)
# MARGIN = 10
# STEP = 10

url = 'https://dtcm.cnnd.vn/tra-cuu-diem-thi-2018.htm'

def format_num(num):
    r = num
    while len(r) < 6:
        r = '0' + r
    return r


def get_student(province_code, student_code):
    params = {
        'keywords': province_code + student_code,
    }

    scores = {
        'Toán:': '',
        'Ngữ văn:': '',
        'NN:': '',
        'Vật lí:': '',
        'Hóa học:': '',
        'Sinh học:': '',
        'Lịch sử:': '',
        'Địa lí:': '',
        'GDCD:': '',
    }

    r = requests.get(url, params)

    if (r.status_code >= 400):
        return

    soup = BeautifulSoup(r.text, 'html.parser')
    p = soup.findAll('p')

    if len(p) == 1:
        return

    line = p[1].text
    items = re.split('\s{2,}', line)
    raw = [ item for item in items if 'KHXH' not in item and 'KHTN' not in item and len(item) > 0 ]

    for i in range(0, len(raw), 2):
        if ('Tiếng' in raw[i]):
            scores['NN:'] = raw[i + 1]
        else: 
            scores[raw[i]] = raw[i + 1]

    csv_score = [params['keywords']] + list(scores.values())
    return csv_score


#  '16', '17', '18', '19', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59', '60', '61', '62', '63', '64', '65'
provinces = ['02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15',]

def crawl(province_code, start_index, end_index):
    global count
    global results

    failed = 0
    for i in tqdm(range(start_index, end_index)):
        count += 1

        student_code = format_num(str(i))
        r = get_student(province_code, student_code)

        if r is not None:
            results.append(r)
            failed = 0
        else:
            failed += 1

        if failed == 200:
            print('Done')
            return

for province_code in provinces:
    count = 0
    results = []
    threads = []

    print(province_code, '...')

    for i in range(0, MARGIN, STEP):
        threads.append(threading.Thread(target=crawl, args=(province_code, i, i + STEP)))

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()

    print(count)

    with open(province_code + '.csv', 'a') as file:
        writer = csv.writer(file, delimiter=',')
        writer.writerows(results)
